(defun extract-races (input-lines)
  (let ((times (mapcar #'parse-integer (str:split " " (string-trim " " (second (str:split ":" (pop input-lines)))) :omit-nulls t)))
        (distances (mapcar #'parse-integer (str:split " " (string-trim " " (second (str:split ":" (pop input-lines)))) :omit-nulls t))))
    (loop :for time :in times
          :for distance :in distances
          :collect `(,time . ,distance))))

(defun extract-race (input-lines)
  (let ((time (parse-integer (apply #'str:concat (str:split " " (string-trim " " (second (str:split ":" (pop input-lines)))) :omit-nulls t))))
        (distance (parse-integer (apply #'str:concat (str:split " " (string-trim " " (second (str:split ":" (pop input-lines)))) :omit-nulls t)))))
    `(,time . ,distance)))

(defun solve-06-a ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/06.txt"))
        (races (extract-races input-lines)))
    (apply #'* (mapcar #'calc-wins races))))

(defun solve-06-b ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/06.txt"))
         (race (extract-race input-lines)))
    (calc-wins race)))

(defun calc-wins (td-pair)
  (let* ((time (coerce (car td-pair) 'double-float))
         (distance (cdr td-pair))
         (d (- (expt time 2) (* 4 distance)))
         (low-values (multiple-value-list (ceiling (- time (sqrt d)) 2)))
         (high-values (multiple-value-list (truncate (+ time (sqrt d)) 2))))
    (when (= (second low-values) 0.0)
      (incf (first low-values)))
    (when (= (second high-values) 0.0)
      (decf (first high-values)))
    (+ (- (first high-values) (first low-values)) 1)))
