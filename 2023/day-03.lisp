(defstruct engine-symbol s x y)

(defstruct part-number number line start end)

(defun extract-parts (input-lines)
  (let* ((rows (length input-lines))
         (cols (length (first input-lines)))
         (array (make-array (list rows cols) :initial-contents input-lines))
         (symbols ())
         (numbers ()))
    (loop :for i :below rows :do
      (let ((number 0)
            (start nil))
        (loop :for j :below cols :do
          (let ((c (aref array i j)))
            (cond
              ;; symbol
              ((and (not (digit-char-p c)) (not (char= #\. c)))
               (push (make-engine-symbol :s c :x j :y i) symbols)
               (when start
                 (push (make-part-number :number number :line i :start start :end (- j 1)) numbers)
                 (setf number 0)
                 (setf start nil)))
              ;; start of number
              ((and (digit-char-p c) (null start))
               (setf start j)
               (setf number (+ (* number 10) (digit-char-p c))))
              ;; inside number
              ((and (digit-char-p c) start)
               (setf number (+ (* number 10) (digit-char-p c))))
              ;; end of number
              ((and (not (digit-char-p c)) start)
               (push (make-part-number :number number :line i :start start :end (- j 1)) numbers)
               (setf number 0)
               (setf start nil))))
            ;; end of line with number
            (when (and start (= j (- cols 1)))
              (push (make-part-number :number number :line i :start start :end j) numbers)))))
    `(,symbols . ,numbers)))

(defun solve-03-a ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/03.txt"))
         (extracted-parts (extract-parts input-lines))
         (symbols (car extracted-parts))
         (numbers (cdr extracted-parts))
         (filtered-numbers (remove-if-not (lambda (num) (dolist (s symbols) (when (neighbours-p num s) (return t)))) numbers))
         (just-numbers (mapcar #'part-number-number filtered-numbers)))
    (apply #'+ just-numbers)))

(defun solve-03-b ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/03.txt"))
         (extracted-parts (extract-parts input-lines))
         (symbols (car extracted-parts))
         (numbers (cdr extracted-parts))
         (stars (remove-if-not (lambda (sym) (char= (engine-symbol-s sym) #\*)) symbols))
         (gears (remove-if-not (lambda (sym) (= (neighbours-count sym numbers) 2)) stars))
         (ratios (mapcar (lambda (gear) (apply #'* (mapcar #'part-number-number (remove-if-not (lambda (num) (neighbours-p num gear)) numbers)))) gears)))
    (apply #'+ ratios)))

(defun neighbours-p (num sym)
  (let ((nx1 (part-number-start num))
        (nx2 (part-number-end num))
        (ny (part-number-line num))
        (sx (engine-symbol-x sym))
        (sy (engine-symbol-y sym)))
    (and (>= sx (- nx1 1))
         (<= sx (+ nx2 1))
         (>= sy (- ny 1))
         (<= sy (+ ny 1)))))

(defun neighbours-count (sym numbers)
  (count-if (lambda (num) (neighbours-p num sym)) numbers))
