(defun solve-01-a ()
  (let ((input-lines (uiop:read-file-lines #p"inputs/01.txt")))
    (apply #'+ (mapcar (lambda (str) (line-to-number str)) input-lines))))

(defun solve-01-b ()
  (let ((input-lines (uiop:read-file-lines #p"inputs/01.txt")))
    (apply #'+ (mapcar (lambda (str) (line-to-number str t)) input-lines))))

(defun line-to-number (line &optional read-words)
  (let ((result (make-string 2))
        (str (if read-words
                 (str:replace-using (loop for i to 9 collect (format nil "~R" i) collect (format nil "~R~A~R" i i i)) line)
                 line)))
    (setf (char result 0) (char str (position-if #'digit-char-p str)))
    (setf (char result 1) (char str (position-if #'digit-char-p str :from-end t)))
    (parse-integer result)))
