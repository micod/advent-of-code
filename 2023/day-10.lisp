(defstruct map-element symbol x y)

(defun solve-10-a ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/10.txt"))
         (rows (length input-lines))
         (cols (length (first input-lines )))
         (map (make-array (list rows cols) :initial-contents input-lines))
         (start (find-start map))
         (pipe-loop (build-loop start map)))
    (/ (length pipe-loop) 2)))

(defun solve-10-b ()
  (let* ((input-lines (uiop:read-file-lines #p"inputs/10-test2.txt"))
         (rows (length input-lines))
         (cols (length (first input-lines )))
         (map (make-array (list rows cols) :initial-contents input-lines))
         (start (find-start map))
         (pipe-loop (build-loop start map))
         (inside nil)
         (area 0))
    (loop :for i :below rows :do
      (loop :for j :below cols
            :for symbol = (aref map i j)
            :for is-loop-pipe = (loop-pipe-p j i pipe-loop)
            :do (cond
                  ((and is-loop-pipe (position symbol "F7JL|"))
                   (setf inside (not inside)))
                  ((and inside (not is-loop-pipe))
                   (format t "~A ~A ~A~%" j i symbol)
                   (incf area))))
      (setf inside nil))
    area))

(defun find-start (map)
  (loop :for i :below (array-dimension map 0) :do
      (loop :for j :below (array-dimension map 1)
            :if (char= (aref map i j) #\S)
              :do (let ((sym (start-symbol j i map)))
                    (setf (aref map i j) sym)
                    (return-from find-start (make-map-element :symbol sym :x j :y i))))))

(defun start-symbol (x y map)
  (let ((northp (position (aref map (- y 1) x) "F|7"))
        (southp (position (aref map (+ y 1) x) "L|J"))
        (westp  (position (aref map y (- x 1)) "F-L"))
        (eastp  (position (aref map y (+ x 1)) "7-J")))
  (cond
    ((and northp southp) #\|)
    ((and northp westp)  #\J)
    ((and northp eastp)  #\L)
    ((and southp westp)  #\7)
    ((and southp eastp)  #\F)
    ((and westp  eastp)  #\-))))

(defun build-loop (start map)
  (let ((current start)
        (prev-vector (starting-vector (map-element-symbol start))))
    (loop :for next-v = (next-vector (map-element-symbol current) prev-vector)
          :for new-x = (+ (map-element-x current) (car next-v))
          :for new-y = (+ (map-element-y current) (cdr next-v))
          :for new-element = (make-map-element :symbol (aref map new-y new-x) :x new-x :y new-y)
          :collect current
          :until (and (= new-x (map-element-x start)) (= new-y (map-element-y start)))
          :do (setf current new-element)
              (setf prev-vector next-v))))

(defun next-vector (symbol prev-vector)
  (case symbol
    (#\| prev-vector)
    (#\- prev-vector)
    (#\L (cond ((equal prev-vector '( 0 .  1)) '( 1 .  0))
               ((equal prev-vector '(-1 .  0)) '( 0 . -1))))
    (#\J (cond ((equal prev-vector '( 0 .  1)) '(-1 .  0))
               ((equal prev-vector '( 1 .  0)) '( 0 . -1))))
    (#\7 (cond ((equal prev-vector '( 1 .  0)) '( 0 .  1))
               ((equal prev-vector '( 0 . -1)) '(-1 .  0))))
    (#\F (cond ((equal prev-vector '(-1 .  0)) '( 0 .  1))
               ((equal prev-vector '( 0 . -1)) '( 1 .  0))))))

(defun starting-vector (symbol)
  (cond
    ((position symbol "F|7") '(0 . -1))
    ((position symbol "LJ")  '(0 .  1))
    ((char= symbol #\-)      '(1 .  0))))

(defun loop-pipe-p (x y pipe-loop)
  (member-if (lambda (e) (and (= (map-element-x e) x) (= (map-element-y e) y))) pipe-loop))
