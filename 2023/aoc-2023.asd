(asdf:defsystem #:aoc-2023
  :serial t
  :description "Advent of Code solutions in Common Lisp"
  :author "Michal Plch"
  :license "GPL"
  :depends-on (#:str)
  :components ((:file "day-01")
               (:file "day-02")
               (:file "day-03")
               (:file "day-04")
               (:file "day-05")
               (:file "day-06")
               (:file "day-07")
               (:file "day-08")
               (:file "day-09")
               (:file "day-10")))
