(defun solve-06-a ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/06.txt"))
         (guard-pos (find-guard input-lines))
         (rows (length input-lines))
         (cols (length (first input-lines)))
         (grid (make-array `(,rows ,cols) :initial-contents input-lines))
         (dirs (list '(0 . -1) '(1 . 0) '(0 . 1) '(-1 . 0))))
    (setf (cdr (last dirs)) dirs)
    (walk-perimeter guard-pos rows cols grid dirs)))

(defun solve-06-b ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/06.txt"))
         (guard-pos (find-guard input-lines))
         (rows (length input-lines))
         (cols (length (first input-lines)))
         (grid (make-array `(,rows ,cols) :initial-contents input-lines))
         (dirs (list '(0 . -1) '(1 . 0) '(0 . 1) '(-1 . 0))))
    (setf (cdr (last dirs)) dirs)
    (loop for i below cols
          sum (loop for j below rows
                    with dest-char = (aref grid j i)
                    count (equal (walk-perimeter guard-pos rows cols grid dirs `(,j . ,i)) 'loops)))))

(defun walk-perimeter (guard-pos rows cols grid dirs &optional (obstacle '(-1 . -1) obstacle-set))
  (when (and obstacle-set (not (char= (aref grid (cdr obstacle) (car obstacle)) #\.)))
    (return-from walk-perimeter 'invalid-obstacle))
  (loop with pos-hash = (make-hash-table :test 'equal)
        with obstacle-hash = (make-hash-table :test 'equal)
        with dir = dirs
        with next-pos = nil
        do (setf next-pos `(,(+ (car guard-pos) (caar dir)) . ,(+ (cdr guard-pos) (cdar dir))))
        if (pos-outside-p next-pos rows cols) do
          (return-from walk-perimeter (1+ (hash-table-count pos-hash)))
        if (or (char= (aref grid (cdr next-pos) (car next-pos)) #\#)
               (equal next-pos obstacle))
          do
             (when (at-object-again-p next-pos (car dir) obstacle-hash)
               (return-from walk-perimeter 'loops))
             (note-direction next-pos (car dir) obstacle-hash)
             (setf dir (cdr dir))
        else
          do
             (setf (gethash guard-pos pos-hash) t)
             (setf guard-pos next-pos)))

(defun find-guard (area)
  (loop for line in area
        for j from 0
        do (loop for c across line
                 for i from 0
                 if (char= c #\^) do (return-from find-guard `(,i . ,j)))))

(defun pos-outside-p (pos rows cols)
  (let ((x (car pos))
        (y (cdr pos)))
    (or (< x 0)
        (>= x cols)
        (< y 0)
        (>= y rows))))

(defun note-direction (object dir table)
  (unless (nth-value 1 (gethash object table))
    (setf (gethash object table) ()))
  (pushnew dir (gethash object table) :test 'equal))

(defun at-object-again-p (object dir table)
  (find dir (gethash object table)))
