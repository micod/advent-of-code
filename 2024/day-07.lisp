(defun solve-07-a ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/07.txt"))
         (equations (mapcar #'parse-equation input-lines))
         (possible-equations (remove-if-not (lambda (equation) (equation-possible-p (car equation) 0 (cdr equation))) equations)))
    (apply '+ (mapcar #'car possible-equations))))

(defun parse-equation (line)
  (let* ((split (str:split ": " line))
         (result (parse-integer (first split)))
         (tokens (str:split " " (second split)))
         (numbers (mapcar #'parse-integer tokens)))
    (cons result numbers)))

(defun equation-possible-p (result intermediate numbers)
  (when (null numbers)
    (return-from equation-possible-p (= result intermediate)))
  (or (equation-possible-p result (+ intermediate (first numbers)) (rest numbers))
      (equation-possible-p result (* intermediate (first numbers)) (rest numbers))))

(defun solve-07-b ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/07.txt"))
         (equations (mapcar #'parse-equation input-lines))
         (possible-equations (remove-if-not (lambda (equation) (equation-possible-b-p (car equation) 0 (cdr equation))) equations)))
    (apply '+ (mapcar #'car possible-equations))))

(defun number-length (number)
  (loop with num = number
        with counter = 1
        if (zerop (floor (/ num (expt 10 counter)))) do
          (return-from number-length counter)
        else do
          (incf counter)))

(defun equation-possible-b-p (result intermediate numbers)
  (when (null numbers)
    (return-from equation-possible-b-p (= result intermediate)))
  (or (equation-possible-b-p result (+ intermediate (first numbers)) (rest numbers))
      (equation-possible-b-p result (* intermediate (first numbers)) (rest numbers))
      (equation-possible-b-p result (+ (* intermediate (expt 10 (number-length (first numbers)))) (first numbers)) (rest numbers))))
