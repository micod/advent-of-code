(defun solve-03-a ()
  (let* ((input (uiop:read-file-string #p"../inputs/2024/03.txt"))
         (matches (ppcre:all-matches-as-strings "mul\\(\\d{1,3},\\d{1,3}\\)" input)))
    (apply '+ (mapcar #'calc-mul matches))))

(defun calc-mul (mul)
  (* (parse-integer mul :start (1+ (position #\( mul)) :junk-allowed t)
     (parse-integer mul :start (1+ (position #\, mul)) :junk-allowed t)))

(defun solve-03-b ()
  (let* ((input (uiop:read-file-string #p"../inputs/2024/03.txt"))
         (matches (ppcre:all-matches-as-strings "mul\\(\\d{1,3},\\d{1,3}\\)" (filter-muls input))))
    (apply '+ (mapcar #'calc-mul matches))))

(defun filter-muls (input)
  (loop with enabled = t
        with cur-pos = 0
        with stream = (make-string-output-stream)
        with next-pos
        do
           (setf next-pos (search (if enabled "don't()" "do()") input :start2 cur-pos))
        when enabled
          do (write-string (subseq input cur-pos next-pos) stream)
        when (null next-pos)
          do (return-from filter-muls (get-output-stream-string stream))
        do (setf cur-pos (+ (if enabled 7 0) next-pos))
           (setf enabled (not enabled))))
