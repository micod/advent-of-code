(asdf:defsystem #:aoc-2024
  :serial t
  :description "Advent of Code solutions in Common Lisp"
  :author "Michal Plch"
  :license "GPL"
  :depends-on (#:str)
  :components ((:file "day-01")))
