(defstruct letter x y char)

(defun solve-04-a ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/04.txt"))
         (rows (length input-lines))
         (cols (length (first input-lines)))
         (grid (make-array `(,rows ,cols) :initial-contents input-lines))
         (patterns (list-xmas-patterns)))
    (loop for i from 0 below cols sum
      (loop for j from 0 below rows sum
        (loop for p in patterns
              count (pattern-at-grid-pos-p grid rows cols p i j))))))

(defun pattern-at-grid-pos-p (grid rows cols p x y)
  (every (lambda (l) (letter-at-grid-origin-p grid rows cols l x y)) p))

(defun letter-at-grid-origin-p (grid rows cols l x y)
  (let* ((dx (+ x (letter-x l)))
         (dy (+ y (letter-y l)))
         (c (letter-char l)))
    (and (>= dx 0)
         (< dx cols)
         (>= dy 0)
         (< dy rows)
         (char= (aref grid dy dx) c))))

(defun list-xmas-patterns ()
  (list (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x  1 :y  0 :char #\M)
              (make-letter :x  2 :y  0 :char #\A)
              (make-letter :x  3 :y  0 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x  1 :y  1 :char #\M)
              (make-letter :x  2 :y  2 :char #\A)
              (make-letter :x  3 :y  3 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x  0 :y  1 :char #\M)
              (make-letter :x  0 :y  2 :char #\A)
              (make-letter :x  0 :y  3 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x -1 :y  1 :char #\M)
              (make-letter :x -2 :y  2 :char #\A)
              (make-letter :x -3 :y  3 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x -1 :y  0 :char #\M)
              (make-letter :x -2 :y  0 :char #\A)
              (make-letter :x -3 :y  0 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x -1 :y -1 :char #\M)
              (make-letter :x -2 :y -2 :char #\A)
              (make-letter :x -3 :y -3 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x  0 :y -1 :char #\M)
              (make-letter :x  0 :y -2 :char #\A)
              (make-letter :x  0 :y -3 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\X)
              (make-letter :x  1 :y -1 :char #\M)
              (make-letter :x  2 :y -2 :char #\A)
              (make-letter :x  3 :y -3 :char #\S))))

(defun solve-04-b ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/04.txt"))
         (rows (length input-lines))
         (cols (length (first input-lines)))
         (grid (make-array `(,rows ,cols) :initial-contents input-lines))
         (patterns (list-x-mas-patterns)))
    (loop for i from 0 below cols sum
      (loop for j from 0 below rows sum
        (loop for p in patterns
              count (pattern-at-grid-pos-p grid rows cols p i j))))))

(defun list-x-mas-patterns ()
  (list (list (make-letter :x  0 :y  0 :char #\A)
              (make-letter :x -1 :y -1 :char #\M)
              (make-letter :x  1 :y -1 :char #\M)
              (make-letter :x  1 :y  1 :char #\S)
              (make-letter :x -1 :y  1 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\A)
              (make-letter :x  1 :y -1 :char #\M)
              (make-letter :x  1 :y  1 :char #\M)
              (make-letter :x -1 :y  1 :char #\S)
              (make-letter :x -1 :y -1 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\A)
              (make-letter :x  1 :y  1 :char #\M)
              (make-letter :x -1 :y  1 :char #\M)
              (make-letter :x -1 :y -1 :char #\S)
              (make-letter :x  1 :y -1 :char #\S))
        (list (make-letter :x  0 :y  0 :char #\A)
              (make-letter :x -1 :y  1 :char #\M)
              (make-letter :x -1 :y -1 :char #\M)
              (make-letter :x  1 :y -1 :char #\S)
              (make-letter :x  1 :y  1 :char #\S))))
