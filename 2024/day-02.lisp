(defun solve-02-a ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/02.txt"))
         (reports (mapcar (lambda (line) (mapcar #'parse-integer (str:split " " line))) input-lines)))
    (count-if #'safep reports)))

(defun solve-02-b ()
  (let* ((input-lines (uiop:read-file-lines #p"../inputs/2024/02.txt"))
         (reports (mapcar (lambda (line) (mapcar #'parse-integer (str:split " " line))) input-lines)))
    (count-if (lambda (report) (or (funcall #'safep report) (funcall #'dampened-safe-p report))) reports)))

(defun safep (levels &optional exclude)
  (loop with direction = 0
        for (a b) on (remove-nth levels exclude) by #'cdr while b
        for diff = (- a b)
        if (zerop direction)
          do (setf direction (signum diff))
        when (or (zerop diff) (not (= direction (signum diff))) (> (abs diff) 3))
          do (return-from safep nil))
  t)

(defun dampened-safe-p (levels)
  (dotimes (i (length levels))
    (when (safep (remove-nth levels i))
        (return-from dampened-safe-p t)))
    nil)

(defun remove-nth (sequence &optional index)
  (if (null index)
      sequence
      (append (subseq sequence 0 index) (subseq sequence (1+ index)))))
